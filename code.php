<?php

/*
[SECTION] Repetition Control Structures

While Loop:

A while loop takes a single condition. FOr as long as the condition is true, the code inside the block will run
*/

function whileLoop() {
	$count = 0;

	while($count <= 5){
		echo $count . '</br>';
		$count++;
	}
}

/*
Do-While Loop
A do-while loop is guaranteed to run at least once before conditions are checked.
*/

function doWhileLoop() {
	$count = 20;

	do{
		echo $count . '</br>';
		$count--;
	}while($count > 0);
}

/*
For Loop

A for loop is a more flexible kind of loop. It consists of the following three parts:
	-The initial value that will track the progression of the loop
	-The condition that will be evaluated and will determine whether the loop will continue running or not
	-The iteration method that indicates how to advance the loop

*/

function forLoop(){
	for($i=0; $i <= 10; $i++){
		echo $i . "</br>";
	}
}


//[SECTION] Array Manipulation

$studentNumbers = array('1923', '1924', '1925', '1926');

//Simple array
//$studentNumbers = ['1923', '1924', '1925', '1926'];
$grades = [98.5, 94.3, 89.2, 90.1];

//Associative Arrays
$gradePeriods = ['firstGrading' => 98.5, 'secondGradin' => 94.3, 'thirdGrading' => 90.1];


//Multi-Dimensional Arrays
$heroes = [
	['Iron Man','Thor', 'Hulk'],
	['Wolverine', 'Cyclops', 'Wonder Woman'],
	['Batman', 'Superman', 'Wonder Woman']
];


//Multi-Dimensional Associative Arrays

$powers = [
	'regular' => ['Repulsor Blast', 'Rocket Puch'],
	'signature' => ['Unibeam']
];

$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

//Searching arrays:
function searchBrand($brand, $brandsArr){
	if(in_array($brand, $brandsArr)){
			return "$brand is in the array.";
	} else {
		return "$brand is NOT in the array.";
	}
}


//strtolower(String)